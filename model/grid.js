const debug = require('debug')('models:grid')
const mercator = require('global-mercator')
const turf = require('@turf/turf')
const wkx = require('wkx')
const hash = require('object-hash')
const { withNeighbour } = require('../plugins')
const Cache = require('app/cache')
const Model = require('app/model')
const BOUNDS = '0102000020110F00000200000000000060F81B73C100000060F81B73C100000060F81B734100000060F81B7341'
const v3 = Math.sqrt(3)
const angle = i => i * Math.PI / 3 + Math.PI / 2

class Grid extends Model {
  static get props () {
    return ({
      id: String,
      datetime: 'integer'
    })
  }
  
  static square (precision) {
    const that = this

    return ({
      key (longlat) {
        const [x, y] = mercator.lngLatToGoogle(longlat, precision)

        return mercator.googleToQuadkey([x, y, precision])
      },
      
      point (quadkey) {
        const [x0, y0, x1, y1] = that.square(precision).polygon(quadkey)
        
        return [(x1 - x0) / 2, (y1 - y0) / 2]
      },

      polygon (quadkey) {
        const key = quadkey.substr(0, precision)
        const google = mercator.quadkeyToGoogle(key)
        
        return mercator.googleToBBox(google)
      }
    })
  }

  static hexagon (precision, bounds = BOUNDS, margin = 0.0) {
    const that = this

    return ({
      key (longlat) {
        const [x, y] = mercator.lngLatToMeters(longlat)
        const { points: [{ x: bx0, y: by0 }, { x: bx1, y: by1 }] } = wkx.Geometry.parse(Buffer.from(bounds, 'hex'))
        let [x0, y0, d0] = [(bx1 - bx0) / 2 + bx0, (by1 - by0) / 2 + by0]
        let [x1, y1, d1] = []
        let r = (bx1 - bx0) / 2
        let rotate
        let idx = 0
        let result = ''

        for (let z = 0; z < precision; z++) {
          rotate = z % 2 ? Math.PI / 6 : 0
          d0 = Math.sqrt(Math.pow(x - x0, 2) + Math.pow(y - y0, 2))
          idx = 0

          for (let i = 0; i < 6; i++) {
            x1 = Math.sin(angle(i) + rotate) * r + x0
            y1 = -Math.cos(angle(i) + rotate) * r + y0
            d1 = Math.sqrt(Math.pow(x - x1, 2) + Math.pow(y - y1, 2))
            // console.log(geom, 'z', z, 'i', i, 'd0', d0, 'd1', d1, 'x', x, 'y', y, 'x0', x0, 'y0', y0, 'x1', x1, 'y1', y1)
            ;([idx, d0] = d0 > d1 ? [i + 1, d1] : [idx, d0])
          }

          if (idx > 0) {
            x0 = Math.sin(angle(idx - 1) + rotate) * r + x0
            y0 = -Math.cos(angle(idx - 1) + rotate) * r + y0
          }

          result += idx

          r /= v3
        }
        // 1420460424001314026362600251
        return result
      },
      
      point (hexkey) {
        const { points: [{ x: bx0, y: by0 }, { x: bx1, y: by1 }] } = wkx.Geometry.parse(Buffer.from(bounds, 'hex'))
        let [x, y, r = x - bx0] = [(bx1 - bx0) / 2 + bx0, (by1 - by0) / 2 + by0]

        for (let z = 0; z < precision; z++) {
          const rotate = z % 2 ? Math.PI / 6 : 0
          const idx = hexkey.charAt(z)

          if (idx > 0) {
            x = Math.sin(angle(idx - 1) + rotate) * r + x
            y = -Math.cos(angle(idx - 1) + rotate) * r + y
          }

          r /= v3
        }

        return mercator.metersToLngLat([x, y])
      },

      polygon (hexkey) {
        const { points: [{ x: bx0 }, { x: bx1 }] } = wkx.Geometry.parse(Buffer.from(bounds, 'hex'))
        const r = (bx1 - bx0) / 2 * Math.pow(v3, -1 * precision)
        const rotate = precision % 2 ? Math.PI / 6 : 0
        const center = mercator.lngLatToMeters(that.hexagon(precision).point(hexkey))

        return [...Array(7)].map((c, i) => mercator.metersToLngLat([
          Math.sin(angle(i) + rotate) * r + center[0],
          -Math.cos(angle(i) + rotate) * r + center[1]
        ]))
      }
    })
  }
  
  static async tile (filter, options = {}) {
    const { x, y, z, column = 'grid'  } = options
    const { connection, schema, table } = this
    const that = this
    
    const data = await withNeighbour(+x, +y, async (x, y) => {
      const key = hash({ connection, schema, table, ...filter, ...options })

      const cache = await Cache.remember(key, Cache.minutes, async () => {
        const query = await that.query()
          .grid(column, x, y, z)
          .limit(10000)
        const { rows = query } = query

        return rows
      })
      
      return cache
    })

    return data
  }

  constructor (data = {}, options = {}) {
    super(data, options)
    this.$properties.id = data.id || this.square(28).key
  }
   
  _prop (shape = 'square', precision = 28) {
    const grid = this.constructor[shape](precision)
    const { $properties, ...props } = { ...this, ...this.$properties }
    const {
      longlat: { 0: l, 1: t, long: ll = l, lng: ln = ll, lat: lt = t } = [],
      long = ln, lng = long, lat = lt, longitude = lng, latitude = lat
    } = props

    return ({
      ...props,
      grid,
      gid: grid.key([longitude, latitude])
    })
  }
  
  square (precision) {
    const { gid: key, grid, ...props } = this._prop('square', precision)

    return ({
      key,
      
      coord () {
        return key
      },
      
      point () {
        return grid.point(key)
      },
      
      polygon () {
        return grid.polygon(key)
      },
      
      geojson (properties) {
        properties = { ...props, ...properties }

        return turf.bboxPolygon(this.polygon(), { properties, id: key })
      }      
    })
  }

  hexagon (precision) {
    const { gid: key, grid, ...props } = this._prop('hexagon', precision)

    return ({
      key,
      
      coord () {
        return this.point().join(':')
      },
      
      point () {
        return grid.point(key)      
      },
      
      polygon () {
        return grid.polygon(key)
      },

      geojson (properties) {
        properties = { ...props, ...properties }

        return turf.polygon([this.polygon()], properties, { id: key })
      }
    })
  }

  geojson ({ shape = 'square', z, zoom = +z, size = +zoom + 4, properties = {} }) {
    return this[shape === 'hgrid' ? 'hexagon' : shape](size).geojson(properties)
  }
  
  toGeoJSON (options) {
    return this.geojson(options)
  }
}

module.exports = require('app/proxy')([global.$?.grid, Grid])
