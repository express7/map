const debug = require('debug')('model:site')
const turf = require('@turf/turf')
const wkx = require('wkx')
const hash = require('object-hash')
const { withNeighbour } = require('../plugins')
const Cache = require('app/cache')
const Model = require('app/model')

class Site extends Model {
  static get props () {
    return ({
      id: String,
      datetime: 'integer',
      properties: ['jsonb'],
      cells: ['jsonb'],
      parents: ['jsonb'],
      longlat: [{ specificType: 'geometry' }]
    })
  }

  constructor (data = {}, options = {}) {
    data.longlat = Array.isArray(data.longlat)
      ? new wkx.Point(data.longlat[0], data.longlat[1], undefined, undefined, 4326).toEwkb().toString('hex')
      : data.longlat 
    
    super(data, { omitAsProp: true, ...options })
  }

  // static async find1 (filter, { x, y, z, minZoom = 16 } = {}) {
    // const query = this.query()
      // .whereRaw(`st_transform(geom, 3857) && st_tileenvelope(${z}, ${x}, ${y})`)

    // if (z <= minZoom) {
      // query.distinctOn([
        // Database.raw(`((st_x(st_transform(longlat, 3857)) + 20037508) / 40075016 * pow(2, ${z}) * pow(2, ${Math.floor(z / 6) + 6}))::int`),
        // Database.raw(`((st_y(st_transform(longlat, 3857)) + 20037508) / 40075016 * pow(2, ${z}) * pow(2, ${Math.floor(z / 6) + 6}))::int`)
      // ])
    // }

    // const data = await query
    // const { rows = data } = data

    // return rows.reduce((a, c) => {
      // const { properties, ...rest } = c
      // return ({ ...a, [c.id]: new this({ ...properties, ...rest }) })
    // }, {})
  // }

  static async tile (filter, options = {}) {
    const { x, y, z, column = 'longlat' } = options
    const { connection, schema, table } = this
    const that = this
    
    const data = await withNeighbour(+x, +y, async (x, y) => {
      const key = hash({ connection, schema, table, ...filter, ...options })

      const cache = await Cache.remember(key, Cache.minutes, async () => {
        const query = await that.query()
          .tileDistinct(column, x, y, z, options)
          .whereRaw(`st_transform(${column}, 3857) && st_tileenvelope(${z}, ${x}, ${y})`)
          .limit(1000)
        const { rows = query } = query

        return rows
      })
      
      return cache
    })

    return data
  }

  center () {
    // return [106.833301, -6.315451]
    const geom = wkx.Geometry.parse(Buffer.from(this.longlat || this.geom, 'hex'))

    const center = geom.constructor.name === 'Point'
      ? geom.toGeoJSON()
      : turf.centroid(geom.toGeoJSON())

    const geojson = geom.srid === 4326
      ? center
      : turf.toWgs84(center)

    return geojson
    // return (geojson.geometry)
    //   ? geojson.geometry.coordinates
    //   : geojson.coordinates
  }

  isAllowed () {
    return true
  }

  draw ({ z, zoom = +z, radius = Math.pow(2, 6 - Math.floor(+zoom / 6)), steps = Math.pow(3, Math.floor(+zoom / 5)), precision } = {}) {
    const color = this.isAllowed() ? 0 : 999
    let { properties, ...props } = this.properties
    properties = { ...properties, ...props, name: this.id, color }

    const geojson = turf.circle(this.center(), +radius / 1000, { steps: this.isAllowed() ? +steps : 3, properties })
    geojson.id = this[this.constructor.keys[0]]

    if (turf.precise && precision) turf.precise(geojson, precision)

    return geojson
  }

  async sectors ({ z, zoom = z, minZoom = 12, maxZoom = 16 }) {
    if (+zoom <= +minZoom) return {}

    // no internal cells data, get from db
    if (!this.cells) this.cells = await Cell.find(this)

    const result = {}
    for (const c of this.cells) {
      const {
        tier2,
        tech: t = tier2, 0: tech = t,
        id: i, 1: id = i,
        type: y, 2: type = y,
        band: b, 3: band = b,
        sector: s = 1, 4: sector = +s,
        azimuth: a = ((+sector - 1) * 120) % 360, 5: azimuth = +a,
        horizontal_beamwidth: hbeam = 0,
        beamwidth = +hbeam
      } = { ...c, ...c.properties }

      const key = +zoom <= +maxZoom
        ? `${tech}${String(+azimuth * 100).padStart(5, '0')}`
        : `${tech}${String(+azimuth * 100).padStart(5, '0')}${String(+beamwidth * 100).padStart(5, '0')}`

      result[key] = new Cell({
        ...(Array.isArray(c) ? {} : c),
        ...c.properties,
        site: this,
        id,
        tech,
        type,
        band,
        sector: +sector,
        azimuth: azimuth === null ? ((+sector - 1) * 120) % 360 : +azimuth
      })
    }

    return result
  }  

}

module.exports = require('app/proxy')([global.$?.site, Site])
