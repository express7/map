const debug = require('debug')('model:cell')
const turf = require('@turf/turf')
const wkx = require('wkx')
const hash = require('object-hash')
const { withNeighbour } = require('../plugins')
const Cache = require('app/cache')
const Model = require('app/model')

class Cell extends Model {
  static get props () {
    return ({
      id: String,
      site_id: [String],
      tech: String,
      datetime: 'integer',
      name: [String]
    })
  }

  static get keys () {
    return ['id', 'tech', 'datetime']
  }
  
  static async tile (filter, options = {}) {
    const { x, y, z, column = 'longlat' } = options
    const { connection, schema, table } = this
    const that = this
    
    const data = await withNeighbour(+x, +y, async (x, y) => {
      const key = hash({ connection, schema, table, ...filter, ...options })

      const cache = await Cache.remember(key, Cache.minutes, async () => {
        const query = await that.query()
          .tileDistinct(column, x, y, z, options)
          .whereRaw(`st_transform(${column}, 3857) && st_tileenvelope(${z}, ${x}, ${y})`)
          .limit(1000)
        const { rows = query } = query

        return rows
      })
      
      return cache
    })

    return data
  }

  constructor (data = {}, options = {}) {
    const { site, ...rest } = data

    super(rest, { omitAsProp: true, ...options })

    this.$properties.site = site
  }
  
  center () {
    return this.site.center()
  }

  isAllowed () {
    return this.site.isAllowed()
  }

  draw ({ z, zoom = +z, steps = Math.pow(2, Math.floor(+zoom / 3)), precision, techMax = 5 } = {}) {
    // do not draw cell on level 12 and below
    if (+zoom < 12) return undefined

    let { site, properties, ...props } = this.properties
    const {
      tech = +String(this.tech).replace(/g+/g, ''),
      radius = (techMax - tech + 3) * Math.pow(2, 6 - Math.floor(+zoom / 6)) * 1.3,
      sector = 1,
      azimuth = ((+sector - 1) * 120) % 360,
      horizontalBeamwidth,
      bmwidth = horizontalBeamwidth,
      beamwidth = (+zoom > 18 ? bmwidth : undefined) || tech * 8
    } = properties

    properties = { ...properties, ...props, name: this.cellName || this.id, sector, azimuth, beamwidth, color: tech }

    const bearings = [+azimuth - (+beamwidth / 2), +azimuth + (+beamwidth / 2)]
    
    // make sector
    let celljson = turf.sector(this.center(), radius / 1000, bearings[0], bearings[1], { steps: +steps, properties })

    // intersect with site
    const geojson = turf.difference(celljson, site.draw({ z }))
    geojson.id = this[this.constructor.keys[0]]

    if (turf.precise && precision) turf.precise(geojson, precision)

    return geojson
  }
}

module.exports = require('app/proxy')([global.$?.cell, Cell])
