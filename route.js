const debug = require('debug')('app:route:map')
const _ = require('app/helpers')
const mercator = require('global-mercator')
const wkx = require('wkx')
const turf = require('@turf/turf')
const render = require('./render')
const mime = require('mime')
const lng = 106.833301
const lat = -6.345551
const zoom = 16
const gps = [lng, lat]
const Site = require('./model/site')
const Cell = require('./model/cell')
const Grid = require('./model/grid')
const { Router, cache, check, validate, version } = require('app/router')
const createError = require('http-errors')

const router = Router({ mergeParams: true })
const v = version({ default: '1.0.0' })

// const srs = '+init=epsg:3857'
const srs = '+proj=longlat +datum=WGS84 +no_defs'
const legend = '0-ffff0099,25-ff000099,50-00ff0099,75-0000ff99'
const value = 'value'
// const xdef = 51463
// const ydef = 33525
// const zdef = 16
const pxdef = 10
const pydef = 10

// const fcache = cache({
//   store: 'file',
//   defaultFormat: 'png',
//   path: path.join(_.tmpPath(), 'map'),
//   minutes: 1
// })

const sample = {
  geojson (x, y, z) {
    const bbox = mercator.googleToBBox([+x, +y, +z])
    
    const polygons = [...Array(2)].map(c => new Grid({ longlat: turf.randomPosition(bbox), value: 100 * Math.random() }))

    polygons[0] = polygons[0].toGeoJSON({ size: +z + 3, properties: { id: 'polygon1'} })
    polygons[1] = polygons[1].toGeoJSON({ size: +z + 6, properties: { id: 'polygon2'}, shape: 'hexagon' })
    
    return polygons
  },
  
  wkb (x, y, z) {
    return sample.geojson(x, y, z).map(c => ({
      geom: wkx.Geometry.parseGeoJSON(c.geometry).toWkb().toString('hex'),
      ...c.properties
    }))
  },

  grid (x, y, z) {
    const bbox = mercator.googleToBBox([+x, +y, +z])

    return [...Array(100)].map(c => ({ longlat: turf.randomPosition(bbox), value: 100 * Math.random() }))
  },

  gridjson (x, y, z) {
    const bbox = mercator.googleToBBox([+x, +y, +z])

    const geojson = (new Grid({ longlat: [(bbox[2] - bbox[0]) / 2 + bbox[0], (bbox[3] - bbox[1]) / 2 + bbox[1]], value: 100 * Math.random() })).toGeoJSON({ size: +z + 5, properties: { id: 'polygon2'}, shape: 'hexagon' })

    const data = [...Array(100)].map(c => ({ longlat: turf.randomPosition(bbox), value: 100 * Math.random() }))

    return { geojson, data }
  }
}

sample.hgrid = sample.grid

module.exports = router
  // site
  .all('/test/site/:z?/:x?/:y?.:format?', v('1.0.0'),
    // validation
    check('z').default(+zoom),
    // check('x').not().isEmpty(),
    // check('y').not().isEmpty(),
    validate,
    // fcache,

    // control
    async (req, res, next) => {
      try {
        if (req.method !== 'GET' && req.method !== 'POST') return next(createError(404))
        
        const {
          z = +zoom,
          x = mercator.lngLatToGoogle(gps, z)[0],
          y = mercator.lngLatToGoogle(gps, z)[1],
          px = pxdef,
          py = pydef,
          format = 'png'
        } = req.all()

        res.type(mime.lookup(format))

        const bbox = mercator.googleToBBox([+x, +y, +z])
        const sites = [...Array(2)].map((c, i) => {
          const id = `site${i + 1}`
          
          const longlat = turf.randomPosition(bbox)

          const site = new Site({ id, longlat, value: 100 * Math.random() })

          const sector2s = [...Array(3)].map((c, i) => {
            const id = `cell${i + 1}`
            const tech = '2g'
            
            const cell = new Cell({ id, site, tech, sector: i + 1, value: 100 * Math.random() })

            return cell.draw({ z })
          })

          const sector3s = [...Array(3)].map((c, i) => {
            const id = `cell${i + 1}`
            const tech = '3g'
            
            const cell = new Cell({ id, site, tech, sector: i + 1, value: 100 * Math.random() })

            return cell.draw({ z })
          })

          const sector4s = [...Array(3)].map((c, i) => {
            const id = `cell${i + 1}`
            const tech = '4g'
            
            const cell = new Cell({ id, site, tech, sector: i + 1, value: 100 * Math.random() })

            return cell.draw({ z })
          })

          const sector5s = [...Array(3)].map((c, i) => {
            const id = `cell${i + 1}`
            const tech = '5g'
            
            const cell = new Cell({ id, site, tech, sector: i + 1, value: 100 * Math.random() })

            return cell.draw({ z })
          })
          
          return [site.draw({ z }), ...sector2s, ...sector3s, ...sector4s, ...sector5s].reverse()
        })
        
        const features = _.flatten(sites)

        const buffer = await render.site(features, { x, y, z, px, py, srs, value, zoom: +z, keyName: 'key', format }) 

        res.set('Cross-Origin-Resource-Policy', 'cross-origin')

        return res[format === 'json' ? 'json' : 'send'](buffer)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  // xml/geojson/wkb/square/hgrid
  .all('/test/:type/:z?/:x?/:y?.:format?', v('1.0.0'),
    // validation
    check('z').default(+zoom),
    check('x').not().isEmpty(),
    check('y').not().isEmpty(),
    // fcache,

    // control
    async (req, res, next) => {
      try {
        if (req.method !== 'GET' && req.method !== 'POST') return next(createError(404))

        const {
          z,
          x = mercator.lngLatToGoogle(gps, z)[0],
          y = mercator.lngLatToGoogle(gps, z)[1],
          px = pxdef,
          py = pydef,
          type = 'geojson',
          format = 'png'
        } = req.all()

        res.type(mime.lookup(format))

        const polygons = sample[type](x, y, z)

        const buffer = await render[type](polygons, { x, y, z, px, py, srs, value, zoom: +z, block: 32, format }) 
  
        res.set('Cross-Origin-Resource-Policy', 'cross-origin')

        return res[format === 'json' ? 'json' : 'send'](buffer)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )
