const debug = require('debug')('controller:map:render:grid')
const geojson = require('./geojson')
const Grid = require('../model/grid')

module.exports = (data, options) => {
  const {
    zoom,
    block = 8,
    step = Math.floor(Math.log2(block)),
    size = +zoom + step,
    shape = 'square'
  } = options
  options.size = size

  const grids = (Array.isArray(data) ? data : [data]).map(c => c instanceof Grid ? c : new Grid(c))  
  const uniqueGrids = [...new Map(grids.map(c => [c[shape](size).coord(), c])).values()]

  return geojson(uniqueGrids.map(c => c[shape](size).geojson()), options)
}
