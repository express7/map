'use strict'

const debug = require('debug')('controller:map:render:grid')
const geojson = require('./grid')
const grid = require('./grid')
const mercator = require('global-mercator')
const turf = require('@turf/turf')

module.exports = (data, options) => {
  const { geojson, data: points } = data
  const { x, y, z, block = 32 } = options
  const [x0, y0, xm, ym] = mercator.googleToBBox([+x, +y, +z])

  // tile bbox to polygon
  const bboxPolygon = turf.bboxPolygon([x0, y0, xm, ym])

  // get each features bbox polygon
  const geojsonBboxPolygons = geojson.features.map(g => turf.bboxPolygon(turf.bbox(g), { properties: { feature: g } }))

  // filter geojson bbox polygons intersects with bbox polygons
  const intersectGeojsonBboxPolygons = geojsonBboxPolygons.filter(g => turf.booleanIntersects(g, bboxPolygon))

  // generate grids from geojson
  // const result = points.filter(({ longlat }) => turf.booleanPointInPolygon(turf.point(longlat), geojson))
  const result = []
  const dx = (xm - x0) / block
  const dy = (ym - y0) / block

  for (let j = 0; j < block; j++) {
    const yj = (y0 + j * dy) + 0.5 * dy

    for (let i = 0; i < block; i++) {
      const xi = (x0 + i * dx) + 0.5 * dx

      for(const { properties: { feature } } of intersectGeojsonBboxPolygons) {
        if (turf.booleanPointInPolygon(turf.point([xi, yj]), feature)) result.unshift({ longlat: [xi, yj], value: -1, style: { fillStyle: 'red' } })
      }
    }  
  }

  return grid(result, { style: { strokeStyle: 'white' }, ...options })
}
