'use strict'

const debug = require('debug')('controller:map:render:site')
const turf = require('@turf/turf')
const spatial = require('wkx')
const Config = require('app/config')
const Database = require('app/database')
const geojson = require('./geojson')

async function buildGeojson ({ x, y, z }) {
  const sites = await Site.find(undefined, { x, y, z })

  const siteJsons = []
  const cellJsons = []

  // site
  for (const skey in sites) {
    const site = sites[skey]

    // draw site
    const siteGeoJSON = site.draw({ z })

    if (siteGeoJSON) {
      const geojsons = {}

      // cell
      if (site.isAllowed()) {
        const cells = await site.sectors({ z })
        for (const ckey in cells) {
          const cell = cells[ckey]

          // draw sector
          const cellGeoJSON = cell.draw({ z })

          if (cellGeoJSON) geojsons[ckey] = cellGeoJSON
        }

        // fix overlap geometries
        for (const i in geojsons) {
          for (const j in geojsons) {
            if (i !== j) {
              const diff = geometry.difference(geojsons[i], geojsons[j])
              if (diff) geojsons[i].geometry = diff.geometry
            }
          }

          const diff = geometry.difference(geojsons[i], siteGeoJSON)
          if (diff) geojsons[i].geometry = diff.geometry
        }

        cellJsons.push(...Object.values(geojsons))
      }

      siteJsons.push(siteGeoJSON)
    }
  }

  return {
    sites: siteJsons,
    cells: cellJsons
  }
}

module.exports = (data, options) => {
  const {
    zoom
  } = options
  
  const features = Array.isArray(data) ? data : [data]
  
  for (let i = 1; i < features.length; i++) {
    for (let j = 0; j < i; j++) {
      try {
        features[i] = turf.booleanIntersects(features[i], features[j])
          ? turf.difference(features[i], features[j])
          : features[i]
      } catch (e) {}
    }    
  }

  return geojson(features.filter(c => !!c), options)
}

