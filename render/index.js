const renders = requireDir('map/render', { exclude: /index\.js$/ })
const render = require('./geojson')

for (const key in renders) render[key] = renders[key]

module.exports = render
