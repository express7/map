'use strict'

const debug = require('debug')('controller:map:render:hgrid')
const grid = require('./grid')

module.exports = (data, options) => {
  const {
    zoom, 
    size = Math.round(+zoom * 1.3 + 4)
  } = options
  options.size = size
  options.grid = 'hexagon'

  return grid(data, options)
}
