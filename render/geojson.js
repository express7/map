'use strict'

const debug = require('debug')('controller:map:render:geojson')
const turf = require('@turf/turf')
const hash = require('object-hash')
const pool = require('../pool')

const geojson = (data, options = {}) => {
  const {
    keyName = 'geojson',
    properties: props
  } = options

  // feature collection
  if (data.type === 'FeatureCollection' && data.features) {
    if (!data.id) data.id = hash(data.features)

    return data
  }
  
  // check array
  const datas = Array.isArray(data) ? data : [data]
  
  // features
  if (datas[0].type = 'Feature') return turf.featureCollection(datas, { id: hash(datas) })
  
  // geometry only
  if (datas[0][keyName].coordinates) {
    const features = datas.map(c => {
      const { [keyName]: geometry, ...properties } = c
      
      return turf.feature(geometry, { ...properties, ...props })
    })
    
    return turf.featureCollection(features, { id: hash(features) })
  }
 
  // feature row
  const features = datas.map(c => {
    const { [keyName]: feature, ...properties } = c
    
    feature.properties = { ...properties, ...feature.properties }

    return feature
  })
  
  return turf.featureCollection(features, { id: hash(features) })
}

// module.exports = (data, options) => require('../canvas').render(geojson(data, options), options)
module.exports = async (data, options) => {
  try {
    const { format = 'png' } = options
    const feature = geojson(data, options)
    const tile = await pool.acquire().promise

    tile.clear()
    const cvs = tile.render(feature, options)

    return require(`../canvas/format/${format}`)(cvs, feature, options) 
  } catch (err) {
    debug(err)
  }
}
