'use strict'

const debug = require('debug')('controller:map:render:wkb')
const wkx = require('wkx')
const turf = require('@turf/turf')
const BOUNDS = '0102000020110F00000200000000000060F81B73C100000060F81B73C100000060F81B734100000060F81B7341'
const geojson = require('./geojson')

const features = (data, options) => {
  const { keyName = 'geom' } = options
 
  return (Array.isArray(data) ? data : [data]).map(c => {
    const { [keyName]: geom, ...properties } = c
    const buffer = typeof geom === 'string' ? new Buffer(geom, 'hex') : geom
    const geometry = wkx.Geometry.parse(buffer).toGeoJSON()

    return turf.feature(geometry, properties)
  })
}

module.exports = async (data, options) => await geojson(features(data, options), options)
