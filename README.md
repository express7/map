# map

[![npm version](https://img.shields.io/npm/v/express-validator.svg)](https://git.immobisp.com/express/map)
[![Build Status](https://img.shields.io/travis/express-validator/express-validator.svg)](https://git.immobisp.com/express/map)
[![Coverage Status](https://img.shields.io/coveralls/express-validator/express-validator.svg)](https://git.immobisp.com/express/map/-/tree/main)

An [express.js](https://github.com/visionmedia/express) middleware for
[World map tile image](https://git.immobisp.com/express/map).

- [Installation](#installation)
- [Documentation](#documentation)
- [Changelog](#changelog)
- [License](#license)

## Installation

```console
npm install git+https://git.immobisp.com/express/map.git
```

Also make sure that you have Node.js 8 or newer in order to use it.

## Documentation

Please refer to the documentation website on https://git.immobisp.com/express/map/-/blob/master/README.md.

## Changelog

Check the [GitLab Releases page](https://git.immobisp.com/express/map/-/blob/master/CHANGELOG.md).

## License

MIT License
