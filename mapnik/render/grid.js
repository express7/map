'use strict'

const debug = require('debug')('controller:map:render:grid')
const Grid = require('../models/grid')
const geojson = require('./geojson')

module.exports = (data, options) => {
  const {
    zoom, 
    size = +zoom + 4
  } = options
  options.size = size

  const grids = (Array.isArray(data) ? data : [data]).map(c => new Grid(c))
  const uniqueGrids = [...new Map(grids.map(c => [c.square(size).coord(), c])).values()]

  return geojson(uniqueGrids.map(c => c.square(size).geojson()), options)
}
