'use strict'

const requireDirectory = require('require-directory')
const _ = require('app/helpers')

module.exports = requireDirectory(module, __dirname, { rename: name => _.camelCase(name) } )
