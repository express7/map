'use strict'

const debug = require('debug')('controller:map:render:hgrid')
const Grid = require('../models/grid')
const geojson = require('./geojson')

module.exports = (data, options) => {
  const {
    zoom, 
    size = Math.round(+zoom * 1.3 + 4)
  } = options
  options.size = size

  const grids = (Array.isArray(data) ? data : [data]).map(c => new Grid(c))
  const uniqueGrids = [...new Map(grids.map(c => [c.hexagon(size).coord(), c])).values()]

  return geojson(uniqueGrids.map(c => c.hexagon(size).geojson()), options)
}
