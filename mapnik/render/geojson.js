'use strict'

const debug = require('debug')('controller:map:render:geojson')
const turf = require('@turf/turf')
const xml = require('./xml')
const hash = require('object-hash')

const feature = (data, options = {}) => {
  const {
    keyName = 'geojson',
    properties: props
  } = options

  // feature collection
  if (data.type === 'FeatureCollection' && data.features) return data
  
  // check array
  const datas = Array.isArray(data) ? data : [data]
  
  // features
  if (datas[0].type = 'Feature') return turf.featureCollection(datas)
  
  // geometry only
  if (datas[0][keyName].coordinates) {
    const features = datas.map(c => {
      const { [keyName]: geometry, ...properties } = c
      
      return turf.feature(geometry, { ...properties, ...props })
    })
    
    return turf.featureCollection(features)
  }
 
  // feature row
  const features = datas.map(c => {
    const { [keyName]: feature, ...properties } = c
    
    feature.properties = { ...properties, ...feature.properties }

    return feature
  })
  
  return turf.featureCollection(features)
}

const geojson = (data, options = {}) => {
  const featureCollection = feature(data, options)
  const {
    srs = '+proj=longlat +datum=WGS84 +no_defs'
    // srs = '+init=epsg:3857'
  } = options
  
  featureCollection.id = hash(featureCollection)
  featureCollection.crs = {
    type: 'name',
    properties: { name: srs }
  }

  return featureCollection
}

module.exports = async (data, options) => await xml(geojson(data, options), options)
