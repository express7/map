'use strict'

const debug = require('debug')('controller:map:render')
const { Map } = require('../builder')
const mercator = require('global-mercator')

// mapnik
let mapnik = {}
for (const modul of ['mapnik', 'mapnik-win']) {
  try {
    mapnik = require(modul)
    break
  } catch (e) {}
}

try {
  mapnik.register_default_input_plugins()
  mapnik.register_default_fonts()
  mapnik.register_system_fonts()
} catch (e) {}

// mapnik proxy

const render = async (options = {}) => {
  const {
    x, y, z,
    width = 256, height = width,
    map = new mapnik.Map(width, height),
    layer, style, xml = (new Map(layer, style, { backgroundColor: 'transparent' })).toXML(), geojson,
    engine = async ({ map, format = 'png' }) => Buffer.from(await map.renderSync({ format }), 'binary')
  } = options

  // load xml data
  await map.fromStringSync(xml)

  // extent to tile
  map.extent = mercator.googleToBBoxMeters([+x, +y, +z])
  // map.zoomAll()

  options.map = map

  // render
  const result = await engine(options)

  // release resources
  // await map.destroy()

  return result
}

module.exports = new Proxy(render, {
  apply (target, thisArg, args) {
    return target.apply(this, args)
  },
  
  get: (target, prop) => {
    return typeof target[prop] !== 'function'
      ? target[prop]
      : (...args) => target[prop].apply(this, args)
  }
})
