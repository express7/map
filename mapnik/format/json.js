'use strict'

const debug = require('debug')('provider:map:query')
const render = require('.')

const queryInfo = ({ map, px, py, layers = { layer: 0 } }) => new Promise((resolve, reject) => {
  // iterate over the first layer returned and get all attribute information for each feature
  return map.queryMapPoint(px, py, layers, (err, results) => {
    if (err) return reject(err)
    
    // console.log(results); // => [{"layer":"layer_name","featureset":{}}]
    const { 0: { featureset } } = results
    var attributes = []
    var feature
    while ((feature = featureset.next())) {
      attributes.push(feature.attributes())
    }
    // console.log(attributes) // => [{"attr_key": "attr_value"}, {...}, {...}]
    return resolve(attributes)
  })
})

module.exports = async (options) => await render({ ...options, engine: queryInfo })
