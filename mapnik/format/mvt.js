'use strict'

const debug = require('debug')('provider:map')
const render = require('.')

const renderVtile = async ({ map, x, y, z }) => {
  // generate vector tile engine
  const mvt = await map.VectorTile(+z, +x, +y)

  const rendered = await renderPromise(map, mvt.instance())

  // release resources
  await mvt.destroy()

  const bufferArray = rendered.getData()
  
  return Buffer.from(bufferArray, 'binary')
}

const renderPromise = (map, engine) => new Promise((resolve, reject) => {
  return map.render(engine, {}, (err, result) => {
    return err ? reject(err) : resolve(result)
  })
})

module.exports = async (options) => await render({ ...options, engine: renderVtile })
