'use strict'

const debug = require('debug')('provider:map:image')
const render = require('.')

const renderImage = async ({ map, format }) => {
  const bufferArray = await map.renderSync({ format })
  
  return Buffer.from(bufferArray, 'binary')
}

module.exports = async (options) => await render({ ...options, engine: renderImage })
