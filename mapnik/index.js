'use strict'

const debug = require('debug')('provider:map:mapnik')
const { Map, Layer, Style, Rule, Symbol, Legend } = require('./builder')

module.exports = {
  test () {
    return true
  },
  
  async render (data, options = {}) {
    const {
      z,
      zoom = +z,
      legend = '0-ffff0099,25-ff000099,50-00ff0099,75-0000ff99',
      value = 'value',
      srs = '+init=epsg:3857' // srs = '+proj=longlat +datum=WGS84 +no_defs'
    } = options
 
    // xml string
    if (typeof data === 'string') return data

    const line = new Symbol('Line', {
      stroke: 'white',
      strokeWidth: Math.pow(2, Math.floor(+zoom / 3)) / 64
    })

    // rule
    const rules = (new Legend([legend, '999-cccccc'])).toJSON().map(c => new Rule(
      `rulegrid${c.value}`,
      [new Symbol('Polygon', { fill: c.color }), line],
      `[mapnik::geometry_type] = polygon and [${value}] ${c.lower}` + (c.upper ? ` and [${value}] ${c.upper}` : '')
    ))

    const style = new Style('style0', rules)

    const layer = [new Layer(`${data.id}-${value}`, data, style, { srs })]

    const result = new Map(layer, style, { backgroundColor: 'transparent' })
    
    const xml = result.toXML()

    const buffer = await require(`./format/${options.format}`)({ ...options, xml })
  
    return buffer
  }
}