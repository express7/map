'use strict'

const jsontoxml = require('jsontoxml')
const _ = require('app/helpers')

module.exports = class Legend {
  constructor (legend, options = {}) {
    if (legend) this.legends = (Array.isArray(legend) ? legend.join(',') : legend)
      .split(',').map(c => c.split('-'))
  }

  toJSON () {
    return [...this.legends]
      .sort((a, b) => +a[0] - +b[0])
      .map((c, i, a) => {
        return ({
          value: +c[0],
          color: `#${c[1]}`,
          sign: '>=',
          lower: `>= ${+c[0]}`,
          upper: a[i + 1] ? `< ${+a[i + 1][0]}` : ''
        })
      })
  }
}
