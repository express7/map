'use strict'

const jsontoxml = require('jsontoxml')
const _ = require('app/helpers')

module.exports = class Style {
  constructor (name, rule, options = {}) {
    this.name = name
    this.rules = Array.isArray(rule) ? rule : [rule]
    this.attrs = options
  }

  toJSON () {
    const { name, attrs, rules } = this
    const rule = rules.map(i => i.toJSON ? i.toJSON() : i)

    return [{
      name: 'Style',
      attrs: { name, ..._.kebabCase(attrs) },
      children: [...rule]
    }]
  }
}
