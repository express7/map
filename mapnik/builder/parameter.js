'use strict'

const jsontoxml = require('jsontoxml')
const _ = require('app/helpers')

module.exports = class Parameter {
  constructor (name, value, options = {}) {
    this.name = name
    this.value = value
    this.attrs = options
  }

  toJSON () {
    const { name, value, attrs } = this

    return [{
      name: 'Parameter',
      attrs: { name, ..._.kebabCase(attrs) },
      text: jsontoxml.cdata(String(value).toString())
    }]
  }
}
