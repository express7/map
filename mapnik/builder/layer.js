'use strict'

const jsontoxml = require('jsontoxml')
const _ = require('app/helpers')
const Parameter = require('./parameter')

module.exports = class Layer {
  constructor (name, geojson, style, options = {}) {
    this.name = name
    this.geojson = geojson
    this.styles = Array.isArray(style) ? style : [style]
    this.attrs = options

    const { crs: { properties: { name: srs = options.srs } = {} } = {} } = geojson || {}
    this.srs = srs
  }

  toJSON () {
    const { name, geojson, styles, attrs, srs } = this
    const children = styles.map(i => i.toJSON ? { StyleName: i.name } : i)

    const Datasource = [
      new Parameter('type', 'geojson'),
      new Parameter('inline', JSON.stringify(geojson))
    ].map(i => i.toJSON ? i.toJSON() : i)
    children.push({ Datasource })

    return [{
      name: 'Layer',
      // attrs: 'name="layer0" srs="+proj=longlat +datum=WGS84 +no_defs"',
      attrs: _.omitBy({ name, srs, ..._.kebabCase(attrs) }, _.isNil),
      children
    }]
  }
}
