'use strict'

const jsontoxml = require('jsontoxml')
const _ = require('app/helpers')

module.exports = class Symbol {
  constructor (type, options = {}, text) {
    this.type = type
    this.attrs = options
    this.text = text
  }

  toJSON () {
    const { type, attrs, text } = this

    return [_.omitBy({
      name: `${_.startCase(_.lowerCase(type))}Symbolizer`,
      attrs: _.kebabCase(attrs),
      text: text ? jsontoxml.cdata(String(text).toString()) : undefined
    }, _.isNil)]
  }
}
