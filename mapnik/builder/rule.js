'use strict'

const jsontoxml = require('jsontoxml')
const _ = require('app/helpers')

module.exports = class Rule {
  constructor (name, symbol, filter, options = {}) {
    this.name = name
    this.filter = filter
    this.symbols = Array.isArray(symbol) ? symbol : [symbol]
    this.attrs = options
  }

  toJSON () {
    const { name, filter, symbols, attrs } = this
    const children = symbols.map(i => i.toJSON ? i.toJSON() : i)
    if (filter) children.push({ name: 'Filter', text: jsontoxml.cdata(filter) })

    return [{
      name: 'Rule',
      attrs: { name, ..._.kebabCase(attrs) },
      children
    }]
  }
}
