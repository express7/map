'use strict'

const jsontoxml = require('jsontoxml')
const _ = require('app/helpers')

module.exports = class Map {
  constructor (layer, style, options = {}) {
    this.layers = Array.isArray(layer) ? layer : [layer]
    this.styles = Array.isArray(style) ? style : [style]
    this.attrs = Object.assign({
      srs: '+init=epsg:3857'
    }, options)
  }

  toJSON () {
    const { layers, styles, attrs } = this
    const styleLayers = layers.reduce((a, c) => ([...a, ...c.styles]), [])
    const children = [...layers, ...styleLayers, ...styles]
      .filter((v, i, s) => s.indexOf(v) === i)
      .map(i => i.toJSON ? i.toJSON() : i)

    return [{
      name: 'Map',
      attrs: _.kebabCase(attrs),
      children
    }]
  }

  toXML ({ prettyPrint = false } = {}) {
    return jsontoxml(this.toJSON(), { xmlHeader: true, docType: 'Map', prettyPrint })
  }
}
