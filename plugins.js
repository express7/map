'use strict'

const _ = require('app/helpers')

module.exports = {
  async withNeighbour (x, y, fn) {
    return _.flatten(await Promise.all([-1, 0, 1].map(
      async (dx) => _.flatten(await Promise.all([-1, 0, 1].map(
        async (dy) => await fn(+x+dx, +y+dy)
      )))
    )))
  }
}
