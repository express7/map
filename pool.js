'use strict'

const debug = require('debug')('provider:map')
const { Pool } = require('tarn')
const Config = require('app/config')
const Tile = require('./canvas')

const createPool = (Resource, options) => new Pool({
  create (cb) {
    return cb(null, new Resource(options))
  },

  destroy (resource) {
    if (resource.destroy) return resource.destroy()
  },
 
  ...Config.get('map.pool', { min: 2, max: 10 }),
})

module.exports = createPool(Tile)
