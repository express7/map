'use strict'

const debug = require('debug')('provider:drawer')
const mercator = require('global-mercator')
const turf = require('@turf/turf')
const proj4 = require('proj4')
const { interpolation } = require('interpolate-json')
const { createCanvas } = require('canvas')

const defStyles = {
  // fillStyle: 'red',
  // lineJoin: 'round',
  // lineWidth: 1,
  // globalAlpha: 1,
  // strokeStyle: 'black',
  // shadowColor: 'black',
  // shadowBlur: 1,
  // shadowOffsetX: 1,
  // shadowOffsetY: 1,
}
      
module.exports = class TileDrawer {
  static project (crs, coord, options) {
    const meter = proj4(crs, "EPSG:3857", coord) // mercator.lngLatToMeters(coord)
    const pixel = mercator.metersToPixels(meter, +options.z).map(c => Math.round(c, 0))
    
    return this.tile(pixel, options)
  }

  static tile (coord, { x, y, z, width = 256, height = +width } = {}) {
    const dy = Math.pow(2, +z) * +height // y diameter
    const rx = +x * +width // x radius + 1 tile shifted
    const ry = +y * +height // y radius + 1 tile shifted
    
    return [coord[0] - rx, dy - coord[1] - ry]
  }  
  
  static LineString (ctx, feature, options) {
    for (const [i, coord] of feature.geometry.coordinates.entries()) {
      const [x, y] = this.project(feature.crs.properties.name, coord, options)

      ctx[i ? 'lineTo' : 'moveTo'](x, y) 
    }

    ctx.stroke()
  }
  
  static Polygon (ctx, feature, options) {
    for (const polygon of feature.geometry.coordinates) {
      for (const [i, coord] of polygon.entries()) {
        const [x, y] = this.project(feature.crs.properties.name, coord, options)

        ctx[i ? 'lineTo' : 'moveTo'](x, y) 
      }

      ctx.closePath()
    }
    
    ctx.fill()
    ctx.stroke()
  }

  static MultiLineString (ctx, feature, options) {
    for (const line of feature.geometry.coordinates) {
      for (const [i, coord] of line.entries()) {
        const [x, y] = this.project(feature.crs.properties.name, coord, options)

        ctx[i ? 'lineTo' : 'moveTo'](x, y) 
      }
    }

    ctx.stroke()
  }

  static MultiPolygon (ctx, feature, options) {
    for (const polygons of feature.geometry.coordinates) {
      for (const polygon of polygons) {
        for (const [i, coord] of polygon.entries()) {
          const [x, y] = this.project(feature.crs.properties.name, coord, options)

          ctx[i ? 'lineTo' : 'moveTo'](x, y) 
        }

        ctx.closePath()
      }
    }

    ctx.stroke()
    ctx.fill()
  }

  static GeometryCollection (ctx, feature, options) {
    for (const geometry of feature.geometries) {
      this[geometry.type](ctx, { type: 'Feature', geometry }, options)
    }
  }

  static caption (ctx, caption, feature, options = {}) {
    // const center = turf.centerOfMass(feature)
    // const [x, y] = this.project(feature.crs.properties.name, turf.getCoord(center), options)

    const { 0: x, 3: y } = turf.bbox(feature)
    const [px, py] = this.project(feature.crs.properties.name, [x, y], options)

    ctx.fillStyle = options.style.fillTextStyle || 'black'
    
    const label = interpolation.expand(caption, { ...options, ...feature.properties })
    ctx.fillText(label, px + 3, py )
  }

  static render (geojson, options) {
    const { format = 'png' } = options
    const tile = new this(options)
    const cvs = tile.render(geojson, options)

    return require(`./format/${format}`)(cvs, geojson, options) 
  }
  
  static test () {
    try {
      const canvas = createCanvas(1, 1)
      
      return true
    } catch (err) {}
  }

  constructor (options = {}) {
    const {
      width = 256,
      height = +width,
      style = {}
    } = options
    
    options.width = +width
    options.height = +height
    
    this.busy = true

    this.canvas = createCanvas(+width, +height)
    this.ctx = this.canvas.getContext('2d')
    this.ctx.font = style.font || '6pt Arial'

    this.busy = false
  }

  render (geojson, options = {}) {
    this.busy = true
    
    // options.style.legend = '0+ffff0099+25 ff000099 50 00ff0099 75 0000ff99'
    const { legend, value, style = {} } = options
    
    // projection
    const defCrs = {
      type: 'name',
      properties: { name: 'EPSG:4326' }
    }

    const { crs = defCrs } = geojson
    
    for (const feature of geojson.features) {
      this.ctx.beginPath()

      // projection
      if (!feature.crs) feature.crs = crs
      
      // style
      if (legend) {
        const legends = legend.split(/[\s,\+]+/)
          .reduce((a, c, i, r) => i % 2 ? [...a, [+r[i - 1], `#${c}`]] : a, [])
          .sort((a, b) => a[0] > b[0])

        style.fillStyle = (legends.slice().reverse().find(([k]) => k <= +feature.properties[value]) || legends[0])[1]
      }

      // put style to ctx
      for (const [key, value] of Object.entries({ ...defStyles, ...style, ...feature.properties.style })) {
        this.ctx[key] = typeof value === 'function'
          ? value(feature.properties, options)
          : value
      }
      
      // draw
      this.constructor[feature.geometry.type](this.ctx, feature, options)
      
      // caption
      const { label, caption = label } = style
      if (caption) this.constructor.caption(this.ctx, caption, feature, options)
    }

    this.busy = false

    return this.canvas
  }

  clear () {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
  }
  
  destroy () {
    this.clear()
  }
}
