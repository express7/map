'use strict'

const debug = require('debug')('provider:map:format')

module.exports = (drawer, geojson, options) => drawer.toBuffer()
